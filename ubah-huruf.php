<!DOCTYPE html>
<html>
<head>
	<title>Ubah huruf</title>
</head>
<body>
<?php
/*Di dalam file tersebut buatlah sebuah function dengan nama ubah_huruf yang menerima parameter berupa string. function akan mereturn string yang berisi karakter-karakter yang sudah diubah dengan karakter setelahnya dalam susunan abjad “abdcde …. xyz”. Contohnya karakter “a” akan diubah menjadi “b” karakter “x” akan berubah menjadi “y”, dst.*/

function ubah_huruf($string){
	     
$alphabet = "abcdefghijklmnopqrstuvwxyz";
$alphabetcap = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
$newstring ="";

$arrString	= str_split($string);
$arrAlphabet = str_split($alphabet);
$arrAlphabetCap = str_split($alphabetcap);


	for ($i = 0; $i < count ($arrString); $i++){
		if(in_array($arrString[$i], $arrAlphabet)){
			$indexString = (array_search($arrString[$i], $arrAlphabet)+1) %26;
			$newstring	.= $arrAlphabet[$indexString];
		}elseif(in_array($arrString[$i], $arrAlphabetCap)){
			$indexString = (array_search($arrString[$i], $arrAlphabetCap)+1) %26;
			$newstring	.= $arrAlphabetCap($indexString); 
			
		} else {
			$newstring .= $string[$i];
		}
	}
	return $newstring. "\n";

}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu









?>
</body>
</html>